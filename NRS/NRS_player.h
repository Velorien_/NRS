#ifndef NRS_PLAYER_H
#define NRS_PLAYER_H

#include "NRS_renderer.h"


class NRS_player{

public:
				  // N    R    D
	int nHealth;  // 15  10   50
	int nDamage;  //  4   4   20
	int nSpeed;   //  2   0    5
	int nReload;  //  2   1   30
	int nRegen;   // 25  25   10

	enum DIRECTION {UP = 0, RIGHT, DOWN, LEFT};
	char cSymbol; // N    R  209
	DIRECTION dPlayerDirection; //kierunek strzelania!

	enum ENTITY {CREATURE = 0, BULLET, DRAGONBREATH};
	ENTITY eKind; //kim jestem i dok�d zmierzam?

	int nCurrHealth;

	int RegenTimer;
	int MoveTimer;
	int ShootTimer;
	int FireDuration; //TYLKO DLA DRAGON BREATH

	//pozycja gracza na EKRANIE!!!!!
	COORD cPlayerPos;

	virtual void GetDamage(int, bool, NRS_renderer*); //dla gracza: zmniejszenie paska zycia na ekranie gry!

	virtual void Shoot(NRS_player**, int) = 0;

	void Move(DIRECTION, char**);

	int TestDest(char); 
};

//klasa postaci smoka
class NRS_dragon : public NRS_player{
public:
	NRS_dragon(int, int);

	void Shoot(NRS_player**, int);
};


//wed�ug zasady "obiekty na mapie to jedna rodzina" - entity zioni�cia
class DragonBreath : public NRS_player{
public:
	DragonBreath(int, int, int);
	void Shoot(NRS_player**, int){}
};

class NRS_nazi : public NRS_player{
public:
	NRS_nazi(int, int);

	void Shoot(NRS_player**, int);

};

class NRS_russki : public NRS_player{
public:
	NRS_russki(int, int);

	void Shoot(NRS_player**, int);
};

class Bullet : public NRS_player{
public:
	Bullet(int, int, int);

	void Shoot(NRS_player**, int){}
};

#endif