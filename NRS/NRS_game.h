#ifndef NRS_GAME_H
#define NRS_GAME_H

//u�atwienie, �eby nie pisa� sto razy pe�nego wy�uskania z tablicy
#define ET EntityTab[i]
#define ETJ EntityTab[j]

#define NEP_X 1
#define NEP_Y 16
#define DEP_X 66
#define DEP_Y 38
#define REP_X 79
#define REP_Y 15

#include "NRS_renderer.h"
#include "NRS_interface.h"
#include "NRS_player.h"
#include <cmath>
#include <cstdlib>
#include <ctime>

class NRS_game{
	enum PLAYER_CHOICE {NAZI = 0, RUSSKI, DRAGON};
	friend class NRS_player; 
	const int EntityQuant; //ile max obiekt�w na mapie?
	int SpawnTimer;
	int SpawnedCreatures;

	bool bLoop;
	INPUT_RECORD InRec; // Jakie�
	DWORD NumRead;		// Cuda
	HANDLE hOut;		// z
	HANDLE hIn;			// WinAPI
	PLAYER_CHOICE pcPChoice;
	NRS_player* PostacGracza; //fanfary!
	NRS_player** EntityTab; //tablica stworze� - serce silnika

public:
	NRS_game();
	NRS_game(int); //ca�kiem niepotrzebny konstruktor
	~NRS_game();

	void GameOver(); //lol, u lose
	void MainGameLoop(NRS_interface*, NRS_renderer*); //myl�ca nazwa, tu s� tylko trzy funkcje
	void HandleStartScreen(); //ekran startowy i wyb�r frakcji
	void HandleGame(NRS_renderer*); //tu si� dzieje ca�a magia! (lol, Magicka, fqfqasa)
	void HandleAI(NRS_player* Current, NRS_renderer* NRSRenderer); //a tu ju� zupe�ne cuda
};

#endif