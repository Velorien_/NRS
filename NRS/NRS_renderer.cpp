#include "NRS_renderer.h"

NRS_renderer::NRS_renderer()
	:
	hOut(GetStdHandle(STD_OUTPUT_HANDLE)),
	ifCommonMap("common_map.txt")
{
	cMapTab = new char*[38];
	for(int i = 0; i < 38; i++)
		cMapTab[i] = new char[79];

	for(int i = 0; i < 38; i++)
		for(int j = 0; j < 79; j++){
			ifCommonMap >> cMapTab[i][j];
		}
}

NRS_renderer::~NRS_renderer(){
	ifCommonMap.close();
	for(int i = 0; i < 38; i++)
		delete [] cMapTab[i];
	delete [] cMapTab;
}

//rysuje ca�� map�
void NRS_renderer::RenderMap(){

	for(int i = 0; i < 38; i++)
		for(int j = 0; j < 79; j++){
			cPos.X = j+1;
			cPos.Y = i+1;
			SetConsoleCursorPosition(hOut, cPos);
			WriteMapPoint(cMapTab[i][j]);
		}


}

//rysuje pole wed�ug podanego chara z tablicy mapy
void NRS_renderer::WriteMapPoint(char cMapPoint){
	/*
	r - road
	v - void
	w - water
	s - sand
	m - moors
	q - swamp
	d - rock
	c - cave
	f - forest
	l - meadow
	g - deep water
	*/

	switch(cMapPoint){
	case 'r':
		SetConsoleTextAttribute(hOut, FWI);
		cout << static_cast<char>(178);
		break;
	case 'v':
		SetConsoleTextAttribute(hOut, 0);
		cout << " ";
		break;
	case 'w':
		SetConsoleTextAttribute(hOut, FBI | BB);
		cout << static_cast<char>(178);
		break;
	case 's':
		SetConsoleTextAttribute(hOut, BY | FYI);
		cout << static_cast<char>(176);
		break;
	case 'm':
		SetConsoleTextAttribute(hOut, FMI);
		cout << static_cast<char>(176);
		break;
	case 'q':
		SetConsoleTextAttribute(hOut, FG);
		cout << "#";
		break;
	case 'd':
		SetConsoleTextAttribute(hOut, BW);
		cout << static_cast<char>(176);
		break;
	case 'c':
		SetConsoleTextAttribute(hOut, FW);
		cout << static_cast<char>(176);
		break;
	case 'f':
		SetConsoleTextAttribute(hOut, FG);
		cout << "Y";
		break;
	case 'l':
		SetConsoleTextAttribute(hOut, FGI);
		cout << static_cast<char>(176);
		break;
	case 'g':
		SetConsoleTextAttribute(hOut, FBI | BB);
		cout << static_cast<char>(177);
		break;
	}
}

//przywraca punkt na mapie po przej�ciu
void NRS_renderer::RestoreMapPoint(COORD cPos){
	SetConsoleCursorPosition(hOut, cPos);
	WriteMapPoint(cMapTab[cPos.Y-1][cPos.X-1]);
}

//rysuje paskek �ycia gracza w zale�no�ci od aktualnego HP
void NRS_renderer::RenderPlayerHealthbar(int nMaxHealth, int CurrHealth){
	double procent = (static_cast<double>(CurrHealth)/static_cast<double>(nMaxHealth))*10;
	COORD pos;
	pos.Y = 5;
	pos.X = 89;
	char draw;
	SetConsoleCursorPosition(hOut, pos);
	SetConsoleTextAttribute(hOut, FRI);
	for(int i = 0; i < 10; i++){
		if(i < procent)
			draw = (char)178;
		else
			draw = (char)176;

		cout << draw;
	}
	
}

//rysuje znacznik kierunku, w kt�ry zwr�cona jest posta� gracza
void NRS_renderer::DrawDirectionMark(int dir){
	SetConsoleTextAttribute(hOut, FWI);
	COORD pos;
	pos.X = 89;
	pos.Y = 9;
	SetConsoleCursorPosition(hOut, pos);
	cout << (char)185;
	pos.X = 90;
	pos.Y = 8;
	SetConsoleCursorPosition(hOut, pos);
	cout << (char)202;
	pos.X = 90;
	pos.Y = 10;
	SetConsoleCursorPosition(hOut, pos);
	cout << (char)203;
	pos.X = 91;
	pos.Y = 9;
	SetConsoleCursorPosition(hOut, pos);
	cout << (char)204;

	SetConsoleTextAttribute(hOut, BWI);
	switch(dir){
	case 3:
		pos.X = 89;
		pos.Y = 9;
		SetConsoleCursorPosition(hOut, pos);
		cout << (char)185;
		break;
	case 0:
		pos.X = 90;
		pos.Y = 8;
		SetConsoleCursorPosition(hOut, pos);
		cout << (char)202;
		break;
	case 2:
		pos.X = 90;
		pos.Y = 10;
		SetConsoleCursorPosition(hOut, pos);
		cout << (char)203;
		break;
	case 1:
		pos.X = 91;
		pos.Y = 9;
		SetConsoleCursorPosition(hOut, pos);
		cout << (char)204;
		break;
	}
		
}

//rysuje stworzenie na mapie (NIE Bullet/Dragonbreath!)
void NRS_renderer::RenderEntity(COORD pos, char sign){
	SetConsoleTextAttribute(hOut, FWI);
	SetConsoleCursorPosition(hOut, pos);
	cout << sign;
}