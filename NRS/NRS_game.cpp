#include "NRS_game.h"


NRS_game::NRS_game():
	hOut(GetStdHandle(STD_OUTPUT_HANDLE)),
	hIn(GetStdHandle(STD_INPUT_HANDLE)),
	pcPChoice(NAZI),
	EntityQuant(100),
	SpawnTimer(0),
	SpawnedCreatures(1)
{
	PostacGracza = NULL;
	EntityTab = new NRS_player*[100];
	for(int i = 0; i < 100; i++)
		EntityTab[i] = NULL;
}

NRS_game::NRS_game(int n):
	hOut(GetStdHandle(STD_OUTPUT_HANDLE)),
	hIn(GetStdHandle(STD_INPUT_HANDLE)),
	pcPChoice(NAZI),
	EntityQuant(n),
	SpawnTimer(0),
	SpawnedCreatures(1)
{
	PostacGracza = NULL;
	EntityTab = new NRS_player*[EntityQuant];
	for(int i = 0; i < EntityQuant; i++)
		EntityTab[i] = NULL;
}

NRS_game::~NRS_game()
{
	if(EntityQuant)
		for(int i = 0; i < EntityQuant; i++)
			delete EntityTab[i];
	else
		for(int i = 0; i < 11; i++)
			delete EntityTab[i];
	delete [] EntityTab;

}

void NRS_game::GameOver(){
	system("CLS");
	cout << endl << endl << endl << endl << endl << endl << endl << endl << endl
		 << endl << "                                          Game Over :: U lose" << endl;
	Sleep(2000);
	exit(0);
}

void NRS_game::MainGameLoop(NRS_interface* NRSInterface, NRS_renderer* NRSRenderer){
	HandleStartScreen();
	system("CLS");
	NRSInterface->DisplayInterface();
	NRSRenderer->RenderMap();
	//temp lines below

	HandleGame(NRSRenderer);
}

void NRS_game::HandleStartScreen(){ //tutaj obslugiwany jest ekran poczatkowy gry
	bool loop = true;
	COORD naz;
	COORD rus;
	COORD dra;
	naz.X = 8;
	naz.Y = 19;
	rus.X = 38;
	rus.Y = 14;
	dra.X = 73;
	dra.Y = 13;
	bool skip_click = false;
	SetConsoleTextAttribute(hOut, FWI);
	SetConsoleCursorPosition(hOut, naz);
	cout << ">>";
	
	while(loop){
		ReadConsoleInput(hIn, &InRec, 1, &NumRead);
		if(skip_click == true)
			skip_click = false;
		else
			skip_click = true;
		
		if(skip_click == true)
			switch(InRec.Event.KeyEvent.wVirtualKeyCode){
			case VK_UP:
			case VK_RIGHT:
				if(pcPChoice == NAZI){
					SetConsoleCursorPosition(hOut, naz);
					cout << "  ";
					SetConsoleCursorPosition(hOut, rus);
					cout << ">>";
					pcPChoice = RUSSKI;
				}else
				if(pcPChoice == RUSSKI){
					SetConsoleCursorPosition(hOut, rus);
					cout << "  ";
					SetConsoleCursorPosition(hOut, dra);
					cout << ">>";
					pcPChoice = DRAGON;
				}else
				if(pcPChoice == DRAGON){
					SetConsoleCursorPosition(hOut, dra);
					cout << "  ";
					SetConsoleCursorPosition(hOut, naz);
					cout << ">>";
					pcPChoice = NAZI;
				}
				break;
			case VK_DOWN:
			case VK_LEFT:
				if(pcPChoice == NAZI){
					SetConsoleCursorPosition(hOut, naz);
					cout << "  ";
					SetConsoleCursorPosition(hOut, dra);
					cout << ">>";
					pcPChoice = DRAGON;
				}else
				if(pcPChoice == RUSSKI){
					SetConsoleCursorPosition(hOut, rus);
					cout << "  ";
					SetConsoleCursorPosition(hOut, naz);
					cout << ">>";
					pcPChoice = NAZI;
				}else
				if(pcPChoice == DRAGON){
					SetConsoleCursorPosition(hOut, dra);
					cout << "  ";
					SetConsoleCursorPosition(hOut, rus);
					cout << ">>";
					pcPChoice = RUSSKI;
				}
				break;
			case VK_RETURN:
				loop = false;
				break;
			}
			
	}
	
	//spawn player!
	if(pcPChoice == DRAGON){
		PostacGracza = new NRS_dragon(DEP_X, DEP_Y);
		EntityTab[0] = PostacGracza;
	}else if(pcPChoice == NAZI){
		PostacGracza = new NRS_nazi(NEP_X, NEP_Y);
		EntityTab[0] = PostacGracza;
	}else if(pcPChoice == RUSSKI){
		PostacGracza = new NRS_russki(REP_X, REP_Y);
		EntityTab[0] = PostacGracza;
	}
	
}

void IterateETJ_fire(COORD pos, NRS_renderer *NRSRenderer, NRS_player* PostacGracza, NRS_player** EntityTab, int EntityQuant, HANDLE hout){
	if(pos.X > 0 && pos.X < 80 && pos.Y > 0 && pos.Y < 39){
		SetConsoleCursorPosition(hout, pos);
		cout << "#";
		for(int j = 0; j < EntityQuant; j++){
			if(ETJ == NULL)
				continue;
			//  czy jest stworzeniem?         czy dane stworzenie znajduje si� w ogniu?
			if((ETJ->eKind == 0) && (ETJ->cPlayerPos.X == pos.X) && (ETJ->cPlayerPos.Y == pos.Y)) //cos sie tu wykrzacza
				ETJ->GetDamage(20, ETJ == PostacGracza, NRSRenderer);
		}
	}
}

bool IterateETJ_bullet(COORD pos, NRS_renderer *NRSRenderer, NRS_player* PostacGracza, NRS_player** EntityTab, int EntityQuant, HANDLE hout){
	bool hit = false;
	if(pos.X > 0 && pos.X < 79 && pos.Y > 0 && pos.Y < 38){
		for(int j = 0; j < EntityQuant; j++){
			if(ETJ == NULL)
				continue;
			//  czy jest stworzeniem?         czy dane stworzenie znajduje si� pod ostrza�em?
			if((ETJ->eKind == 0) && (ETJ->cPlayerPos.X == pos.X) && (ETJ->cPlayerPos.Y == pos.Y)){
				ETJ->GetDamage(4, ETJ == PostacGracza, NRSRenderer);
				hit = true;
			}
		}
	}
	return hit;
}

void IterateETJ_quench(COORD pos, NRS_renderer *NRSRenderer, NRS_player* PostacGracza, NRS_player** EntityTab, int EntityQuant, HANDLE hout){
	if(pos.X > 0 && pos.X < 80 && pos.Y > 0 && pos.Y < 39){
		for(int j = 0; j < EntityQuant; j++){
			if(ETJ == NULL)
				continue;
			if(ETJ->cPlayerPos.X == pos.X && ETJ->cPlayerPos.Y == pos.Y && ETJ->eKind != 2){
				SetConsoleCursorPosition(hout, pos);
				SetConsoleTextAttribute(hout, FWI);
				cout << ETJ->cSymbol;
			}else
				NRSRenderer->RestoreMapPoint(pos);
		}
	}
}

void MoveFunction(NRS_player** EntityTab, int EntityQuan, NRS_player* Current, NRS_renderer* NRSrenderer){
	bool canmove = true;
	COORD pos;
	pos.X = Current->cPlayerPos.X;
	pos.Y = Current->cPlayerPos.Y;

	Current->Move(Current->dPlayerDirection, NRSrenderer->cMapTab);

	for(int i = 0; i < EntityQuan; i++){
		if(ET == NULL)
			continue;
		if(Current->cPlayerPos.X == ET->cPlayerPos.X && Current->cPlayerPos.Y == ET->cPlayerPos.Y && ET != Current){
			canmove = false;
			break;
		}
	}

	if(canmove){
		NRSrenderer->RestoreMapPoint(pos);
		NRSrenderer->RenderEntity(Current->cPlayerPos, Current->cSymbol);
	}else{
		Current->cPlayerPos.X = pos.X;
		Current->cPlayerPos.Y = pos.Y;
	}
}

void NRS_game::HandleAI(NRS_player* Current, NRS_renderer* NRSrenderer){
	double dDist;
	
	dDist = sqrt((double)((PostacGracza->cPlayerPos.X-Current->cPlayerPos.X)*(PostacGracza->cPlayerPos.X-Current->cPlayerPos.X)+(PostacGracza->cPlayerPos.Y-Current->cPlayerPos.Y)*(PostacGracza->cPlayerPos.Y-Current->cPlayerPos.Y)));
	
	if(Current->cSymbol == char(209)){ //if smok
		if(PostacGracza->cPlayerPos.X == Current->cPlayerPos.X || PostacGracza->cPlayerPos.Y == Current->cPlayerPos.Y){ //zbli�anie si� i zioni�cie
			if((PostacGracza->cPlayerPos.X == Current->cPlayerPos.X) && (PostacGracza->cPlayerPos.Y > Current->cPlayerPos.Y))
				Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(2);
			else if((PostacGracza->cPlayerPos.X == Current->cPlayerPos.X) && (PostacGracza->cPlayerPos.Y < Current->cPlayerPos.Y))
				Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(0);
			else if((PostacGracza->cPlayerPos.Y == Current->cPlayerPos.Y) && (PostacGracza->cPlayerPos.X > Current->cPlayerPos.X))
				Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(1);
			else
				Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(3);
			
			if(dDist <= 3){
				if(Current->ShootTimer == 0)
					Current->Shoot(EntityTab, EntityQuant);
			}else if(Current->MoveTimer == 0){
				MoveFunction(EntityTab, EntityQuant, Current, NRSrenderer);
			}
		}
		else{ // zbli�anie si� do gracza
			if(abs(PostacGracza->cPlayerPos.X-Current->cPlayerPos.X) <= abs(PostacGracza->cPlayerPos.Y-Current->cPlayerPos.Y)){
				if(PostacGracza->cPlayerPos.X < Current->cPlayerPos.X)
					Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(3);
				else
					Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(1);
			}else if(abs(PostacGracza->cPlayerPos.X-Current->cPlayerPos.X) > abs(PostacGracza->cPlayerPos.Y-Current->cPlayerPos.Y)){
				if(PostacGracza->cPlayerPos.Y < Current->cPlayerPos.Y)
					Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(0);
				else
					Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(2);
			}

			if(Current->MoveTimer == 0){
				MoveFunction(EntityTab, EntityQuant, Current, NRSrenderer);
			}
		}
	}else //if ca�a reszta - gra obs�uguje tylko rozpoznanie bojem
		if(PostacGracza->cPlayerPos.X == Current->cPlayerPos.X || PostacGracza->cPlayerPos.Y == Current->cPlayerPos.Y){ //zbli�anie si� i strza�
			//wykrywanie kierunku gracza
			if((PostacGracza->cPlayerPos.X == Current->cPlayerPos.X) && (PostacGracza->cPlayerPos.Y > Current->cPlayerPos.Y))
				Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(2);
			else if((PostacGracza->cPlayerPos.X == Current->cPlayerPos.X) && (PostacGracza->cPlayerPos.Y < Current->cPlayerPos.Y))
				Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(0);
			else if((PostacGracza->cPlayerPos.Y == Current->cPlayerPos.Y) && (PostacGracza->cPlayerPos.X > Current->cPlayerPos.X))
				Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(1);
			else
				Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(3);
			
			if(dDist <= 10){
				if(Current->ShootTimer == 0)
					Current->Shoot(EntityTab, EntityQuant);
			}else if(Current->MoveTimer == 0){
				MoveFunction(EntityTab, EntityQuant, Current, NRSrenderer);
			}
		}
		else{ // zbli�anie si� do gracza
			if(abs(PostacGracza->cPlayerPos.X-Current->cPlayerPos.X) <= abs(PostacGracza->cPlayerPos.Y-Current->cPlayerPos.Y)){
				if(PostacGracza->cPlayerPos.X < Current->cPlayerPos.X)
					Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(3);
				else
					Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(1);
			}else if(abs(PostacGracza->cPlayerPos.X-Current->cPlayerPos.X) > abs(PostacGracza->cPlayerPos.Y-Current->cPlayerPos.Y)){
				if(PostacGracza->cPlayerPos.Y < Current->cPlayerPos.Y)
					Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(0);
				else
					Current->dPlayerDirection = static_cast<NRS_player::DIRECTION>(2);
			}

			if(Current->MoveTimer == 0){
				MoveFunction(EntityTab, EntityQuant, Current, NRSrenderer);
			}
		}

		///////////
}

void NRS_game::HandleGame(NRS_renderer* NRSRenderer){

	NRSRenderer->RenderEntity(PostacGracza->cPlayerPos, PostacGracza->cSymbol);
	NRSRenderer->RenderPlayerHealthbar(PostacGracza->nHealth, PostacGracza->nCurrHealth); //pokazuje pocz�tkowy pasek �ycia
	bool skip_loop = false; //zmienna zapobiegaj�ca dwukrotnemu wykrywaniu eventu klawiatury


	while(bLoop){
		DWORD dwaitresult;

		srand(static_cast<int>(time(NULL)));

		//oczekiwanie na input gracza
		dwaitresult = WaitForSingleObject(hIn, 50);
		switch(dwaitresult){
		case WAIT_TIMEOUT:
			InRec.Event.KeyEvent.wVirtualKeyCode = VK_RETURN;
			break;
		case WAIT_OBJECT_0:
			ReadConsoleInput(hIn, &InRec, 1, &NumRead);
			break;
		}


		if(skip_loop){
			skip_loop = false;
			continue;
		}else
			skip_loop = true;

		//spawn enemies
		int it = 0;
		if(SpawnTimer == 0 && SpawnedCreatures < 20){
			if(pcPChoice == NAZI){
				switch(rand() % 2){
				case 0:
					for(;it < EntityQuant; it++)
						if(EntityTab[it] == NULL){
							EntityTab[it] = new NRS_dragon(DEP_X, DEP_Y);
							NRSRenderer->RenderEntity(EntityTab[it]->cPlayerPos, EntityTab[it]->cSymbol);
							break;
						}
						SpawnTimer = 100;
					break;
				case 1:
					for(;it < EntityQuant; it++)
						if(EntityTab[it] == NULL){
							EntityTab[it] = new NRS_russki(REP_X, REP_Y);
							NRSRenderer->RenderEntity(EntityTab[it]->cPlayerPos, EntityTab[it]->cSymbol);
							break;
						}
						SpawnTimer = 100;
				}
			}else if(pcPChoice == RUSSKI){
				switch(rand() % 2){
				case 0:
					for(;it < EntityQuant; it++)
						if(EntityTab[it] == NULL){
							EntityTab[it] = new NRS_dragon(DEP_X, DEP_Y);
							NRSRenderer->RenderEntity(EntityTab[it]->cPlayerPos, EntityTab[it]->cSymbol);
							break;
						}
						SpawnTimer = 100;
					break;
				case 1:
					for(;it < EntityQuant; it++)
						if(EntityTab[it] == NULL){
							EntityTab[it] = new NRS_nazi(NEP_X, NEP_Y);
							NRSRenderer->RenderEntity(EntityTab[it]->cPlayerPos, EntityTab[it]->cSymbol);
							break;
						}
						SpawnTimer = 100;
				}
			}else if(pcPChoice == DRAGON){
				switch(rand() % 2){
				case 0:
					for(;it < EntityQuant; it++)
						if(EntityTab[it] == NULL){
							EntityTab[it] = new NRS_nazi(NEP_X, NEP_Y);
							NRSRenderer->RenderEntity(EntityTab[it]->cPlayerPos, EntityTab[it]->cSymbol);
							break;
						}
						SpawnTimer = 100;
					break;
				case 1:
					for(;it < EntityQuant; it++)
						if(EntityTab[it] == NULL){
							EntityTab[it] = new NRS_russki(REP_X, REP_Y);
							NRSRenderer->RenderEntity(EntityTab[it]->cPlayerPos, EntityTab[it]->cSymbol);
							break;
						}
						SpawnTimer = 100;
				}
			}
			SpawnedCreatures++;
		}else
			SpawnTimer--;

		//obsluga inputu gracza
		switch(InRec.Event.KeyEvent.wVirtualKeyCode){
		case VK_UP:
			PostacGracza->dPlayerDirection = static_cast<NRS_player::DIRECTION>(0);
			if(PostacGracza->MoveTimer == 0)
				MoveFunction(EntityTab, EntityQuant, PostacGracza, NRSRenderer);
			NRSRenderer->DrawDirectionMark(static_cast<NRS_player::DIRECTION>(0));
			break;
		case VK_DOWN:
			PostacGracza->dPlayerDirection = static_cast<NRS_player::DIRECTION>(2);
			if(PostacGracza->MoveTimer == 0)
				MoveFunction(EntityTab, EntityQuant, PostacGracza, NRSRenderer);
			NRSRenderer->DrawDirectionMark(static_cast<NRS_player::DIRECTION>(2));
			break;
		case VK_LEFT:
			PostacGracza->dPlayerDirection = static_cast<NRS_player::DIRECTION>(3);
			if(PostacGracza->MoveTimer == 0)
				MoveFunction(EntityTab, EntityQuant, PostacGracza, NRSRenderer);
			NRSRenderer->DrawDirectionMark(static_cast<NRS_player::DIRECTION>(3));
			break;
		case VK_RIGHT:
			PostacGracza->dPlayerDirection = static_cast<NRS_player::DIRECTION>(1);
			if(PostacGracza->MoveTimer == 0)
				MoveFunction(EntityTab, EntityQuant, PostacGracza, NRSRenderer);
			NRSRenderer->DrawDirectionMark(static_cast<NRS_player::DIRECTION>(1));
			break;
		case VK_SPACE:
			if(PostacGracza->ShootTimer == 0)
				PostacGracza->Shoot(EntityTab, EntityQuant);
			break;
		case VK_ESCAPE:
			bLoop = false;
			break;
		}


		//#define ET EntityTab[i] !!!
		for(int i = 0; i < EntityQuant; i++){
			if(ET == NULL)
				continue;
			switch(ET->eKind){
			case 0: //creature
				if(ET->nCurrHealth <= 0){
					if(ET == PostacGracza) //tu umieramy
						GameOver();
					NRSRenderer->RestoreMapPoint(ET->cPlayerPos);
					delete ET;
					ET = NULL;
					SpawnedCreatures--;
					continue;
				}

				if(ET->MoveTimer > 0) //timery, timery
					ET->MoveTimer--;
				if(ET->ShootTimer > 0)
					ET->ShootTimer--;
				if(ET->RegenTimer > 0)
					ET->RegenTimer--;
				if(ET->RegenTimer == 0 && ET->nCurrHealth < ET->nHealth){
					ET->nCurrHealth++;
					ET->RegenTimer = ET->nRegen;
					if(ET == PostacGracza)
						NRSRenderer->RenderPlayerHealthbar(ET->nHealth, ET->nCurrHealth);
				}
				//no to czas na jakie� AI
				if(ET != PostacGracza)
					HandleAI(ET, NRSRenderer);
				

				break;
			case 1: //bullet
				//czy trafi� w kogo�? A mo�e wylecia� za plansz�?
				if(IterateETJ_bullet(ET->cPlayerPos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut) || (ET->cPlayerPos.X == 0 || ET->cPlayerPos.X == 80 || ET->cPlayerPos.Y == 0 || ET->cPlayerPos.Y == 39)){
					if(!ET->FireDuration){
						switch(ET->dPlayerDirection){
							case 0:
								ET->cPlayerPos.Y++;
								NRSRenderer->RestoreMapPoint(ET->cPlayerPos);
								ET->cPlayerPos.Y--;
								break;
							case 1:
								ET->cPlayerPos.X--;
								NRSRenderer->RestoreMapPoint(ET->cPlayerPos);
								ET->cPlayerPos.X++;
								break;
							case 2:
								ET->cPlayerPos.Y--;
								NRSRenderer->RestoreMapPoint(ET->cPlayerPos);
								ET->cPlayerPos.Y++;
								break;
							case 3:
								ET->cPlayerPos.X++;
								NRSRenderer->RestoreMapPoint(ET->cPlayerPos);
								ET->cPlayerPos.X--;
								break;
						}
					}
					delete ET;
					ET = NULL;
				}else{

					if(ET->FireDuration){
						ET->FireDuration = 0;
					}else{
						switch(ET->dPlayerDirection){
						case 0:
							ET->cPlayerPos.Y++;
							NRSRenderer->RestoreMapPoint(ET->cPlayerPos);
							ET->cPlayerPos.Y--;
							break;
						case 1:
							ET->cPlayerPos.X--;
							NRSRenderer->RestoreMapPoint(ET->cPlayerPos);
							ET->cPlayerPos.X++;
							break;
						case 2:
							ET->cPlayerPos.Y--;
							NRSRenderer->RestoreMapPoint(ET->cPlayerPos);
							ET->cPlayerPos.Y++;
							break;
						case 3:
							ET->cPlayerPos.X++;
							NRSRenderer->RestoreMapPoint(ET->cPlayerPos);
							ET->cPlayerPos.X--;
							break;
						}
						SetConsoleCursorPosition(hOut, ET->cPlayerPos);
						SetConsoleTextAttribute(hOut, FWI);
						cout << ET->cSymbol;
					}
				
					switch(ET->dPlayerDirection){
					case 0:
						ET->cPlayerPos.Y--;
						break;
					case 1:
						ET->cPlayerPos.X++;
						break;
					case 2:
						ET->cPlayerPos.Y++;
						break;
					case 3:
						ET->cPlayerPos.X--;
						break;
					}
				}
				break;
			case 2: //dragonbreath
				COORD pos = ET->cPlayerPos;
				
				if(ET->FireDuration > 0){ //mo�na zmieni� na == 0 i pooptymalizowa�, ale po co? Niech zbieraj� obra�enia od ognia! :D
					ET->FireDuration--;
					SetConsoleTextAttribute(hOut, FRI);
					IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);

					switch(ET->dPlayerDirection){
					case 0:
						pos.Y--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						break;
					case 1:
						pos.X++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						break;
					case 2:
						pos.Y++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						break;
					case 3:
						pos.X--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X--;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_fire(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						break;
					}

				}else{
					IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);

					switch(ET->dPlayerDirection){
					case 0:
						pos.Y--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						break;
					case 1:
						pos.X++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						break;
					case 2:
						pos.Y++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						break;
					case 3:
						pos.X--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X--;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.Y++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						pos.X++;
						IterateETJ_quench(pos, NRSRenderer, PostacGracza, EntityTab, EntityQuant, hOut);
						break;
					}
					
					delete ET;	//usuwa dragonbreath z tablicy stworze�
					ET = NULL;
					
				}
				break;
			}

		}
		
	}

}