#ifndef MAPFNS_H
#define MAPFNS_h

#include <iostream>
#include <Windows.h>
#include <fstream>

using namespace std;

class MapFns{

	char cMapTab[79][38];
	HANDLE hOut;
	HANDLE hIn;
	COORD cPos;
	COORD cMouse;
	INPUT_RECORD InRec;
	DWORD NumRead;
	char cDraw;
	ofstream mapa;

public:
	MapFns();
	void DrawFrame();
	void MapDraw();
};

#endif