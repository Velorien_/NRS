#include "MapFns.h"

MapFns::MapFns()
	:
	hOut(GetStdHandle(STD_OUTPUT_HANDLE)),
	hIn(GetStdHandle(STD_INPUT_HANDLE)),
	mapa("mapa.txt")
{
	for(int i = 0; i < 38; i++)
		for(int j = 0; j < 79; j++)
			cMapTab[j][i] = ' ';
}

void MapFns::DrawFrame(){
	SMALL_RECT srBuf = {};
	COORD cBuf;

	cBuf.X = 101;
	cBuf.Y = 41;
	srBuf.Bottom = 40;
	srBuf.Left = 0;
	srBuf.Right = 100;
	srBuf.Top = 0;

	CONSOLE_CURSOR_INFO ConCurInf;
	ConCurInf.bVisible = false;
	ConCurInf.dwSize = 1;
	SetConsoleCursorInfo(hOut, &ConCurInf);
	SetConsoleTitle(TEXT("MapDraw"));
	SetConsoleScreenBufferSize(hOut, cBuf);
	SetConsoleWindowInfo(hOut, TRUE, &srBuf);

	SetConsoleTextAttribute(hOut, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);

	for(int i = 1; i < 40; i++){
		cDraw = (char)186;
		cPos.Y = i;
		cPos.X = 0;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
		cPos.X = 100;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
	}

	for(int i = 1; i <= 100; i++){
		cDraw = (char)205;
		cPos.X = i;
		cPos.Y = 0;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
		cPos.Y = 39;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
	}

	cPos.X = cPos.Y = 0;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)201;
	cout << cDraw;

	cPos.X = 0;
	cPos.Y = 39;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)200;
	cout << cDraw;

	cPos.X = 100;
	cPos.Y = 0;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)187;
	cout << cDraw;

	cPos.X = 100;
	cPos.Y = 39;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)188;
	cout << cDraw;
	
	/* pionowa kreska oddzielajaca mape od menu bocznego */
	cPos.X = 80;
	cPos.Y = 0;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)203;
	cout << cDraw;

	cPos.Y = 39;
	SetConsoleCursorPosition(hOut, cPos);
	cDraw = (char)202;
	cout << cDraw;
	
	cDraw = (char)186;

	for(int i = 1; i < 39; i++){
		cPos.Y = i;
		SetConsoleCursorPosition(hOut, cPos);
		cout << cDraw;
	}
	SetConsoleTextAttribute(hOut, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN);
	cPos.X = 81;
	cPos.Y = 9;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "r - road";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "v - void";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "w - water";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "s - sand";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "m - moors";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "q - swamp";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "d - rock";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "c - cave";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "f - forest";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "l - meadow";
	cPos.Y++;
	SetConsoleCursorPosition(hOut, cPos);
	cout << "g - depths";

}

void MapFns::MapDraw(){
	COORD beg;
	beg.X = beg.Y = 0;
	bool loop = true;
	/*
	r - road
	v - void
	w - water
	s - sand
	m - moors
	q - swamp
	d - rock
	c - cave
	f - forest
	l - meadow
	g - deep water
	*/
	while(loop){
		ReadConsoleInput(hIn, &InRec, 1, &NumRead);

		switch(InRec.EventType){
		case KEY_EVENT:
			if(InRec.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
				loop = false;

			SetConsoleTextAttribute(hOut, FOREGROUND_INTENSITY | FOREGROUND_GREEN);
			switch(InRec.Event.KeyEvent.uChar.AsciiChar){
			case 'r':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "r - road";
				cDraw = 'r';
				break;
			case 'v':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "v - void";
				cDraw = 'v';
				break;
			case 'w':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "w - water";
				cDraw = 'w';
				break;
			case 's':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "s - sand";
				cDraw = 's';
				break;
			case 'm':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "m - moors";
				cDraw = 'm';
				break;
			case 'q':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "q - swamp";
				cDraw = 'q';
				break;
			case 'd':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "d - rock";
				cDraw = 'd';
				break;
			case 'c':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "c - cave";
				cDraw = 'c';
				break;
			case 'f':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "f - forest";
				cDraw = 'f';
				break;
			case 'l':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "l - meadow";
				cDraw = 'l';
				break;
			case 'g':
				cPos.X = 81;
				cPos.Y = 7;
				SetConsoleCursorPosition(hOut, cPos);
				cout << "            ";
				SetConsoleCursorPosition(hOut, cPos);
				cout << "g - depths";
				cDraw = 'g';
				break;
			}
			break;
		case MOUSE_EVENT:
			if(InRec.Event.MouseEvent.dwEventFlags == MOUSE_MOVED){
				cMouse.X = InRec.Event.MouseEvent.dwMousePosition.X;
				cMouse.Y = InRec.Event.MouseEvent.dwMousePosition.Y;
				SetConsoleCursorPosition(hOut, beg);
				cout << "       ";
				SetConsoleCursorPosition(hOut, beg);
				cout << cMouse.X << " " << cMouse.Y;
			}
			if(InRec.Event.MouseEvent.dwButtonState == MK_LBUTTON && cMouse.X > 0 && cMouse.X < 80 && cMouse.Y > 0 && cMouse.Y < 39){
				SetConsoleCursorPosition(hOut, cMouse);
				SetConsoleTextAttribute(hOut, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
				cout << cDraw;
				cMapTab[cMouse.X-1][cMouse.Y-1] = cDraw;
			}
			break;
		}
	}


	for(int i = 0; i < 38; i++){
		for(int j = 0; j < 79; j++){
			mapa << cMapTab[j][i];
		}
		mapa << endl;
	}
}